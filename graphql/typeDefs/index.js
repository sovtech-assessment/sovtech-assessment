const { gql } = require("apollo-server");

module.exports = gql`
  type ChuckResponse {
    id: String!
    joke: String!
    url: String!
    icon_url: String!
  }

  type Query {
    getRandomJokes(category: String!): ChuckResponse!
    getCategoryList: [String!]!
  }
`;
