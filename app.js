const { ApolloServer, AuthenticationError } = require("apollo-server");
const fetch = require("node-fetch");
const typeDefs = require("./graphql/typeDefs");

const baseURL = "https://api.chucknorris.io/";

const resolvers = {
  Query: {
    getRandomJokes: async (req, res, context) => {
      const response = await fetch(
        `${baseURL}jokes/random?category=${res.category}`
      );
      const data = await response.json();
      return { joke: data.value };
    },
    getCategoryList: async (req, res, context) => {
      const response = await fetch(`${baseURL}jokes/categories`);
      const data = await response.json();
      return data;
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req: { headers } }) => {
    if (!headers.authorization || headers.authorization === "") {
      throw new AuthenticationError("No authorization header found");
    }

    const response = headers.authorization.split(" ");

    if (response[0] !== "Bearer") {
      throw new AuthenticationError("No Bearer found");
    }

    if (!response[1] || "") {
      throw new AuthenticationError("unauthenticated");
    }
  }
});

server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
